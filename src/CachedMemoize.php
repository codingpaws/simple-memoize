<?php

namespace CodingPaws\SimpleMemoize;

use CodingPaws\SimpleMemoize\Util\KeyFinder;
use DateInterval;
use Illuminate\Support\Facades\Cache;

trait CachedMemoize
{
    protected function memoizeCached($key, $callable = null, $ttl = null)
    {
        if (is_callable($key)) {
            $callable = $key;
            $key = KeyFinder::find();
        }

        $id = $this->id ?? null;

        $key = str_replace('\\', '_', self::class . ($id ? ":$id" : '') . ":$key");

        return Cache::get($key, function () use ($key, $callable, $ttl) {
            $value = $callable();

            Cache::put($key, $value, $ttl ?: DateInterval::createFromDateString('1 hour'));

            return $value;
        });
    }
}
