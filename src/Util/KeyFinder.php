<?php

namespace CodingPaws\SimpleMemoize\Util;

use Exception;

final class KeyFinder
{
    public static function find(): string
    {
        $exception = new Exception();
        return $exception->getTrace()[2]['function'];
    }
}
