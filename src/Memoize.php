<?php

namespace CodingPaws\SimpleMemoize;

use CodingPaws\SimpleMemoize\Util\KeyFinder;

trait Memoize
{
    private array $__cp_memoize_cache = [];

    protected function memoize($key, $callable = null)
    {
        if (is_callable($key)) {
            $callable = $key;
            $key = KeyFinder::find();
        }

        if ($this->isMemoized($key)) {
            return $this->__cp_memoize_cache[(string) $key];
        }

        return $this->__cp_memoize_cache[(string) $key] = $callable();
    }

    public function isMemoized(string $key): bool
    {
        return array_key_exists($key, $this->__cp_memoize_cache);
    }

    public function clearMemoization(string $key): void
    {
        if ($this->isMemoized($key)) {
            unset($this->__cp_memoize_cache[$key]);
        }
    }
}
