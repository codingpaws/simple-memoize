<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class MemoizeTest extends TestCase
{
    public function subject(): Calculator
    {
        return new Calculator(100000);
    }

    public function testInitialValue()
    {
        $calculator = $this->subject();
        $this->assertEquals(5, $calculator->log10());
    }

    public function testMemoization()
    {
        $calculator = $this->subject();
        $this->assertEquals(5, $calculator->log10());
        $this->assertEquals(5, $calculator->log10());
    }

    public function testIsMemoized()
    {
        $calculator = $this->subject();

        $this->assertFalse($calculator->isMemoized('log10'));
        $this->assertEquals(5, $calculator->log10());
        $this->assertTrue($calculator->isMemoized('log10'));
        $this->assertEquals(5, $calculator->log10());
        $this->assertTrue($calculator->isMemoized('log10'));
    }

    public function testClearMemoization()
    {
        $calculator = $this->subject();

        $this->assertEquals(5, $calculator->log10());
        $calculator->clearMemoization('log10');
        $this->assertFalse($calculator->isMemoized('log10'));
        $this->assertEquals(5, $calculator->log10());
        $this->assertTrue($calculator->isMemoized('log10'));
    }

    public function testWithCallback()
    {
        $calculator = $this->subject();
        $this->assertFalse($calculator->isMemoized('multiply:15,7'));
        $this->assertEquals(105, $calculator->multiply(15, 7));
        $this->assertTrue($calculator->isMemoized('multiply:15,7'));
        $calculator->clearMemoization('multiply:15,7');
        $this->assertFalse($calculator->isMemoized('multiply:15,7'));
    }

    public function testInstancesDontOverlap()
    {
        $calculator_a = $this->subject();
        $calculator_b = $this->subject();

        $this->assertFalse($calculator_a->isMemoized('log10'));
        $this->assertFalse($calculator_b->isMemoized('log10'));
        $this->assertEquals(5, $calculator_a->log10());
        $this->assertTrue($calculator_a->isMemoized('log10'));
        $this->assertFalse($calculator_b->isMemoized('log10'));
        $this->assertEquals(5, $calculator_b->log10());
        $this->assertTrue($calculator_a->isMemoized('log10'));
        $this->assertTrue($calculator_b->isMemoized('log10'));
    }
}
