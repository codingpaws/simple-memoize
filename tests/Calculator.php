<?php

namespace Tests;

use CodingPaws\SimpleMemoize\Memoize;

class Calculator
{
    use Memoize;

    public function __construct(public int $number)
    {
    }

    public function log10(): int
    {
        return $this->memoize(fn () => log10($this->number));
    }

    public function multiply(int $a, int $b): int
    {
        return $this->memoize("multiply:$a,$b", fn () => $a * $b);
    }
}
