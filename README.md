# simple-memoize

`codingpaws/simple-memoize` is a PHP library for simple, trait-based
memoization (i.e. caching return values per class). Although many PHP
memoization dependencies exist, there is none that can be used very
easily like this:

```php
use CodingPaws\SimpleMemoize\Memoize;

class Calculator
{
    use Memoize;

    public function __construct(public int $base) {
    }

    public function pow10(): int
    {
        return $this->memoize(fn () => pow(10, $this->exponent));
    }
}

$calculator = new Calculator(1234);
$calculator->pow10(); // 3.0913151596972
$calculator->pow10(); // will not be calculated again
```
